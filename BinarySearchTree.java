import java.util.LinkedList;
import java.util.Queue;
//Brenna Cantwell -- CS124 Data Structures--My friend did this, and they're horrible.
// BinarySearchTree class
//
// CONSTRUCTION: with no initializer
//
// ******************PUBLIC OPERATIONS*********************
// void insert( x )       --> Insert x
// void remove( x )       --> Remove x
// boolean contains( x )  --> Return true if x is present
// Comparable findMin( )  --> Return smallest item
// Comparable findMax( )  --> Return largest item
// boolean isEmpty( )     --> Return true if empty; else false
// void makeEmpty( )      --> Remove all items
// void printTree( )      --> Print tree in sorted order
// ******************ERRORS********************************
// Throws UnderflowException as appropriate

/**
 * Implements an unbalanced binary search tree. Note that all "matching" is
 * based on the compareTo method.
 * 
 * @author Mark Allen Weiss
 */
public class BinarySearchTree<AnyType extends Comparable<? super AnyType>> {
	/**
	 * Construct the tree.
	 */
	public BinarySearchTree() {
		root = null;
	}

	/**
	 * Insert into the tree; duplicates are ignored.
	 * 
	 * @param x the item to insert.
	 */
	public void insert(AnyType x) {
		root = insert(x, root);
	}

	/**
	 * Remove from the tree. Nothing is done if x is not found.
	 * 
	 * @param x the item to remove.
	 */
	public void remove(AnyType x) {
		root = remove(x, root);
	}

	/**
	 * Find the smallest item in the tree.
	 * 
	 * @return smallest item or null if empty.
	 */
	public AnyType findMin() {
		if (isEmpty())
			throw new UnderflowException();
		return findMin(root).element;
	}

	/**
	 * Find the largest item in the tree.
	 * 
	 * @return the largest item of null if empty.
	 */
	public AnyType findMax() {
		if (isEmpty())
			throw new UnderflowException();
		return findMax(root).element;
	}
	/**
	 * Find an item in the tree.
	 * 
	 * @param x the item to search for.
	 */
	public AnyType find(AnyType x) {
		if (isEmpty())
			throw new UnderflowException();
		return find(x, root);
	}

	/**
	 * Find an item in the tree.
	 * 
	 * @param x the item to search for.
	 * @return true if not found.
	 */
	public boolean contains(AnyType x) {
		return contains(x, root);
	}


	/**
	 * Make the tree logically empty.
	 */
	public void makeEmpty() {
		root = null;
	}

	/**
	 * Test if the tree is logically empty.
	 * 
	 * @return true if empty, false otherwise.
	 */
	public boolean isEmpty() {
		return root == null;
	}

	/**
	 * Print the tree contents in sorted order.
	 */
	public void printTree() {
		if (isEmpty())
			System.out.println("Empty tree");
		else
			printTree(root);
	}

	/**
	 * Print the tree contents in post order.
	 */
	public void printPostOrderTree() {
		if (isEmpty())
			System.out.println("Empty tree");
		else
			printTreePostOrder(root);
	}
	/**
	 * Print the tree contents in pre-order.
	 */
	public void printPreOrderTree() {
		if (isEmpty())
			System.out.println("Empty tree");
		else
			printTreePreOrder(root);
	}
/**
 * Print the leaves, or nodes with no children
 * 
 */
	public int leaves() 
    {
        return leaves(root);
    }
/**
 * Print the total number of nodes in the tree
 * @return the node number
 */
	public int nodes() {
		return nodes(root);
	}
  /**
   * Print the number of nodes bearing two children
   * @return the total number of full nodes
   */
	public int fullNodes() {
		return fullNodes(root);
	}
/**
 * Print the nodes of the tree by level
 */
	public void levelOrder() {
		levelOrder(root);
	}
	
/**
 * Private method to compute the total number of nodes in a tree
 * @param t the node that roots the subtrees
 * @return the total count of nodes
 * runs in O(n) time
 */
	private int nodes(BinaryNode<AnyType> t) {

		int count = 1; 
		if (t.right != null) {
			count += nodes(t.right);
		} 
		if (t.left != null) {
			count += nodes(t.left);
		}
		return count;
	}
	
	
	    
/**
 * Private method to count the number of leaf nodes (bearing no children)	  
 * @param t the node that roots the subtrees
 * @return the number of leaves in the tree
 * runs in O(n) time
 */
	private int leaves(BinaryNode<AnyType> t) {
		if (t == null) {
			return 0;
		}
		if (t.left == null && t.right == null) {
			return 1;
		} else {
			return leaves(t.left) + leaves(t.right);
		}
	}
 /**
  * Private method to count the number of full nodes (bearing two children) 
  * @param t the node that roots the subtrees
  * @return the number of full nodes in the tree
  * runs in O(n) time
  */
	private int fullNodes(BinaryNode<AnyType> t) {
		if (t == null) {
			return 0;
		}
		if (t.left != null && t.right != null) {
			return 1;
		} else {
			return fullNodes(t.left) + fullNodes(t.right);
		}
	}
   
  /**
   * Internal method to print the nodes in the range of two objects, k1 and k2
   * @param k1, the minimum of the range
   * @param k2 the maximum of the range
   * @param root, the node that roots the subtrees
   * runs in O(n) time
   */
	private void range(AnyType k1, AnyType k2, BinaryNode<AnyType> root) {
		if (root == null) {
			return;
		}
		if (k1.compareTo(root.element) < 0) {
			range(k1, k2, root.left);
		}
		if (k1.compareTo(root.element) <= 0 && k2.compareTo(root.element) >= 0) {
			System.out.println(root.element);
		}
		if (k2.compareTo(root.element) > 0) {
			range(k1, k2, root.right);
		}

	}
	/**
	 * Internal method to insert into a subtree.
	 * 
	 * @param x the item to insert.
	 * @param t the node that roots the subtree.
	 * @return the new root of the subtree.
	 *
	private BinaryNode<AnyType> insert(AnyType x, BinaryNode<AnyType> t) {
		if (t == null)
			return new BinaryNode<>(x, null, null);

		int compareResult = x.compareTo(t.element);

		if (compareResult < 0)
			t.left = insert(x, t.left);
		else if (compareResult > 0)
			t.right = insert(x, t.right);
		else
			; // Duplicate; do nothing
		return t;
	}
*/
	
   
 /**
  * Internal method to insert into the tree non-recursively
  * @param x an object to be inserted
  * @param t the node that roots the subtree
  * @return t
  * runs in O(n) time
  */
	private BinaryNode<AnyType> insert(AnyType x, BinaryNode<AnyType> t) {
		BinaryNode<AnyType> node = new BinaryNode<AnyType>(x, null, null);
		if (t == null)
			return node;
		BinaryNode<AnyType> parent = new BinaryNode<AnyType>(null, null, null);
		BinaryNode<AnyType> current = t;
		while (current != null) {
			parent = current;
			if (current.element.compareTo(x) < 0) {
				current = current.right;
			} else {
				current = current.left;
			}
		}
		if (parent.element.compareTo(x) < 0) {
			parent.right = node;

		} else {
			parent.left = node;
		}
		return t;
	}
	/**
	 * Internal method to remove from a subtree.
	 * 
	 * @param x the item to remove.
	 * @param t the node that roots the subtree.
	 * @return the new root of the subtree.
	 */
	private BinaryNode<AnyType> remove(AnyType x, BinaryNode<AnyType> t) {
		if (t == null)
			return t; // Item not found; do nothing

		int compareResult = x.compareTo(t.element);

		if (compareResult < 0)
			t.left = remove(x, t.left);
		else if (compareResult > 0)
			t.right = remove(x, t.right);
		else if (t.left != null && t.right != null) // Two children
		{
			t.element = findMin(t.right).element;
			t.right = remove(t.element, t.right);
		} else
			t = (t.left != null) ? t.left : t.right;
		return t;
	}

	/**
	 * Internal method to find the smallest item in a subtree.
	 * 
	 * @param t the node that roots the subtree.
	 * @return node containing the smallest item.
	 */
	private BinaryNode<AnyType> findMin(BinaryNode<AnyType> t) {
		if (t == null)
			return null;
		else if (t.left == null)
			return t;
		return findMin(t.left);
	}

	/**
	 * Internal method to find the largest item in a subtree.
	 * 
	 * @param t the node that roots the subtree.
	 * @return node containing the largest item.
	 */
	private BinaryNode<AnyType> findMax(BinaryNode<AnyType> t) {
		if (t != null)
			while (t.right != null)
				t = t.right;

		return t;
	}

	/**
	 * Internal method to find an item in a subtree.
	 * 
	 * @param x is item to search for.
	 * @param t the node that roots the subtree.
	 * @return node containing the matched item.
	 */
	private boolean contains(AnyType x, BinaryNode<AnyType> t) {
		if (t == null)
			return false;

		int compareResult = x.compareTo(t.element);

		if (compareResult < 0)
			return contains(x, t.left);
		else if (compareResult > 0)
			return contains(x, t.right);
		else
			return true; 
	}

	/**
	 * Internal method to print a subtree in sorted order.
	 * 
	 * @param t the node that roots the subtree.
	 *            
	 */
	private void printTree(BinaryNode<AnyType> t) {
		if (t != null) {
			printTree(t.left);
			System.out.println(t.element);
			printTree(t.right);
		}
	}
	
	/**
	 * Internal method to print a subtree in pre-order.
	 * 
	 * @param t the node that roots the subtree.
	 */
	private void printTreePreOrder(BinaryNode<AnyType> t) {
		if (t != null) {
			System.out.println(t.element);
			printTreePreOrder(t.left);
			printTreePreOrder(t.right);
		}
	}

	/**
	 * Internal method to print a subtree in post-order.
	 * 
	 * @param t the node that roots the subtree.
	 */
	private void printTreePostOrder(BinaryNode<AnyType> t) {
		if (t != null) {
			printTreePostOrder(t.left);
			printTreePostOrder(t.right);
			System.out.println(t.element);
			
		}
	}
	/**
	 * Internal method to print the nodes of a tree by level, using a queue.
	 * 
	 * @param t the node that roots the subtree.
	 * runs in O(n) time
	 */
	private void levelOrder(BinaryNode<AnyType> t) {
		Queue q = new LinkedList();
		int level = 0;
		q.add(t);

		if (t == null) {
			return;
		}

		while (!q.isEmpty()) {
			level = q.size();
			while (level > 0) {
				BinaryNode<AnyType> n = (BinaryNode<AnyType>) q.remove();
				System.out.print(n.element + " ");
				if (n.left != null) {
					q.add(n.left);
				}
				if (n.right != null) {
					q.add(n.right);
				}
				level--;
			}
			System.out.println("");
		}
	}
	/**
	 * Internal iterative method to find an element in a tree using a queue
	 * @param x the element to be found
	 * @param t the node that roots the subtree.
	 */
	private AnyType find(AnyType x, BinaryNode<AnyType> t) {
		if (t != null) {
			Queue<BinaryNode<AnyType>> q = new LinkedList<BinaryNode<AnyType>>();
			// add root to the queue
			q.add(t);
			while (!q.isEmpty()) {
				t = q.poll();
				// check if current node has the element we are looking for
				if (t.element == x) {
					System.out.print("Here it is: ");
					return t.element;
				}

				// add children to the queue
				if (t.left != null) {
					q.add(t.left);
				}
				if (t.right != null) {
					q.add(t.right);
				}
			}
			// if reached here, means we have not found the element
			System.out.print("Element not found.");
		}
		// if root is null, return null
		return null;

	}

    /**
	 * Internal method to compute height of a subtree.
	 * 
	 * @param t the node that roots the subtree.
	 */
	private int height(BinaryNode<AnyType> t) {
		if (t == null)
			return -1;
		else
			return 1 + Math.max(height(t.left), height(t.right));
	}

	// Basic node stored in unbalanced binary search trees
	private static class BinaryNode<AnyType> {
		// Constructors
		BinaryNode(AnyType theElement) {
			this(theElement, null, null);
		}

		BinaryNode(AnyType theElement, BinaryNode<AnyType> lt, BinaryNode<AnyType> rt) {
			element = theElement;
			left = lt;
			right = rt;
		}

		AnyType element; // The data in the node
		BinaryNode<AnyType> left; // Left child
		BinaryNode<AnyType> right; // Right child
	}

	/** The tree root. */
	private BinaryNode<AnyType> root;

// Test program
	public static void main(String[] args) {
//Empty Tree
		BinarySearchTree<Integer> emptyTree = new BinarySearchTree<>();
		emptyTree.printTree();
		emptyTree.printPreOrderTree();
		System.out.println(emptyTree.find(3, emptyTree.root));
//Integer Tree		
		BinarySearchTree<Integer> u = new BinarySearchTree<>();
		u.insert(4);
		u.insert(3);
		u.insert(2);
		u.insert(7);
		u.insert(11);
		u.insert(5);
		u.insert(12);
		u.printTree();
		u.printPreOrderTree();
		System.out.println(u.find(3, u.root));
		System.out.println(u.nodes());
		System.out.println(u.leaves());
		System.out.println(u.fullNodes());
		u.range(2, 11, u.root);
		u.levelOrder(u.root);
//Question 4 test on Fig. 4.72
		BinarySearchTree<Integer> FourSevenTwo = new BinarySearchTree<>();
		FourSevenTwo.insert(10);
		FourSevenTwo.insert(4);
		FourSevenTwo.insert(11);
		FourSevenTwo.insert(2);
		FourSevenTwo.insert(6);
		FourSevenTwo.insert(12);
		FourSevenTwo.insert(1);
		FourSevenTwo.insert(3);
		FourSevenTwo.insert(5);
		FourSevenTwo.insert(8);
		FourSevenTwo.insert(13);
		FourSevenTwo.insert(7);
		FourSevenTwo.insert(9);
		FourSevenTwo.levelOrder(FourSevenTwo.root);
//String tree
		BinarySearchTree<String> stringTree = new BinarySearchTree<>();
		stringTree.insert("It");
		stringTree.insert("works");
		stringTree.insert("grapes");
		stringTree.insert("for");
		stringTree.insert("free");
		stringTree.insert("strings");
		stringTree.printTree();
		stringTree.printPreOrderTree();
		stringTree.printPostOrderTree();
		System.out.println(stringTree.find("3", stringTree.root));
		System.out.println(stringTree.find("works", stringTree.root));
		System.out.println(stringTree.nodes());
		System.out.println(stringTree.leaves());
		System.out.println(stringTree.fullNodes());
		stringTree.range("Apple", "Jellybean", stringTree.root);
		stringTree.levelOrder(stringTree.root);
	}
}
